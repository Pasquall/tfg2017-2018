﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PropertyData.cs" company="MyNeihborhood">
//   Pascual Maestre  MyNeihborhood.
// </copyright>
// <summary>
//   The user data.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MyNeighborhood.Data
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    using MyNeighborhood.Models;

    /// <summary>
    /// The user data.
    /// </summary>
    public class PropertyData : AbstractMyNeighborhoodData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PropertyData"/> class. 
        /// </summary>
        /// <param name="contextContainer">
        /// The context container.
        /// </param>
        public PropertyData(DbContextContainer contextContainer)
        {
            this.Initialize(contextContainer);
        }

        #region Models
        
        /// <summary>
        /// The get all properties.
        /// </summary>
        /// <param name="includeUser">
        /// The include User.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public object GetAllProperties(bool includeUser = true)
        {
            var query = this.DbContext.Properties
                .Include(p => p.Comunity)
                .Include(p => p.Type);

            if (includeUser)
            {
                query = query.Include(p => p.UserInfo).Include(p => p.UserInfo.ApplicationUser);
            }

            return query.ToList();
        }

        /// <summary>
        /// The get all properties by comunity.
        /// </summary>
        /// <param name="comunityId">
        /// The comunity id.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public object GetAllPropertiesByComunity(Guid? comunityId)
        {
            var query = this.DbContext.Properties
                .Include(p => p.Comunity)
                .Include(p => p.Type)
                .Include(p => p.UserInfo)
                .Include(p => p.UserInfo.ApplicationUser)
                .Where(p => p.ComunityId == comunityId);

            return query.ToList();
        }

        /// <summary>
        /// The get all properties by neighbor.
        /// </summary>
        /// <param name="neighborId">
        /// The neighbor id.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public object GetAllPropertiesByNeighbor(string neighborId)
        {
            var query = this.DbContext.Properties
                .Include(p => p.Comunity)
                .Include(p => p.Type)
                .Include(p => p.UserInfo)
                .Include(p => p.UserInfo.ApplicationUser)
                .Where(p => p.UserInfo.ApplicationUser.Id == neighborId);

            return query.ToList();
        }

        /// <summary>
        /// The get property by id.
        /// </summary>
        /// <param name="propertyId">
        /// The property id.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public Property GetPropertyById(Guid? propertyId)
        {
            var query = this.DbContext.Properties
                .Include(p => p.Comunity)
                .Include(p => p.Type)
                .Include(p => p.UserInfo)
                .Include(p => p.UserInfo.ApplicationUser)
                .Where(p => p.Id == propertyId);

            return query.Single();
        }

        /// <summary>
        /// The save.
        /// </summary>
        /// <param name="property">
        /// The app user.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool Save(Property property)
        {
            try
            {
                if (property != null)
                {
                    this.DbContext.Entry(property).State = EntityState.Modified;

                    this.DbContext.SaveChanges();
                }

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        #endregion
    }
}