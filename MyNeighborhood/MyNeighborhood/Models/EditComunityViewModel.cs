﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyNeighborhood.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// The edit comunity view model.
    /// </summary>
    public class EditComunityViewModel
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the street adress.
        /// </summary>
        [Required]
        public string StreetAdress { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        public Status Status { get; set; }
        
        /// <summary>
        /// Gets or sets the president id.
        /// </summary>
        [ForeignKey("President")]
        public int? PresidentId { get; set; }
    }
}