﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HomeController.cs" company="MyNeighborhood">
//   Pascual Maestre. MyNeighborhood.
// </copyright>
// <summary>
//   Defines the HomeController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MyNeighborhood.Controllers
{
    using System.Globalization;
    using System.Threading;
    using System.Web.Mvc;

    /// <summary>
    /// The home controller.
    /// </summary>
    public class HomeController : MyNeighborhood
    {
        /// <summary>
        /// The index.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Index()
        {
            return this.View();
        }

        /// <summary>
        /// The about.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult About()
        {
            return this.View();
        }

        /// <summary>
        /// The contact.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Contact()
        {
            return this.View();
        }
        
        /// <summary>
        /// The change language.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string ChangeLanguage()
        {
            var lang = this.Request.Form["lang"];
            if (string.IsNullOrEmpty(lang))
            {
                return "0";
            }

            Thread.CurrentThread.CurrentCulture = new CultureInfo(lang);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(lang);
            
            // record language with session everytime 
            this.Session["CurrentLang"] = lang;
            return "1";
        }
    }
}