﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserData.cs" company="MyNeihborhood">
//   Pascual Maestre  MyNeihborhood.
// </copyright>
// <summary>
//   The user data.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MyNeighborhood.Data
{
    using System;
    using System.Linq;

    using MyNeighborhood.Models;

    /// <summary>
    /// The user data.
    /// </summary>
    public class UserData : AbstractMyNeighborhoodData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserData"/> class. 
        /// </summary>
        /// <param name="contextContainer">
        /// The context container.
        /// </param>
        public UserData(DbContextContainer contextContainer)
        {
            this.Initialize(contextContainer);
        }

        #region Models

        /// <summary>
        /// The create infoes.
        /// </summary>
        /// <param name="userInfo">
        /// The user info.
        /// </param>
        /// <returns>
        /// The <see cref="UserInfo"/>.
        /// </returns>
        public UserInfo CreateInfoes(UserInfo userInfo)
        {
            try
            {
                if (userInfo != null)
                {
                    this.DbContext.UserInfoes.Add(userInfo);
                    this.DbContext.SaveChanges();

                    return userInfo;
                }
            }
            catch (Exception e)
            {
                // ignored
            }

            return null;
        }

        /// <summary>
        /// The save.
        /// </summary>
        /// <param name="appUser">
        /// The app user.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool Save(ApplicationUser appUser)
        {
            try
            {
                if (appUser != null)
                {
                    return this.DbContextContainer.ApplicationUserData.Save(appUser);
                }

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        #endregion

        /// <summary>
        /// The get user info by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="UserInfo"/>.
        /// </returns>
        public UserInfo GetUserInfoById(int? id)
        {
            var query = this.DbContext.UserInfoes.Where(p => p.Id == id);

            if (query.Any())
            {
                return query.Single();
            }

            return null;
        }
    }
}