﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ComunityData.cs" company="MyNeihborhood">
//   Pascual Maestre  MyNeihborhood.
// </copyright>
// <summary>
//   The comunity data.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MyNeighborhood.Data
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Web.Mvc;

    using MyNeighborhood.App_GlobalResources;
    using MyNeighborhood.Models;

    /// <summary>
    /// The user data.
    /// </summary>
    public class ComunityData : AbstractMyNeighborhoodData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ComunityData"/> class. 
        /// </summary>
        /// <param name="contextContainer">
        /// The context container.
        /// </param>
        public ComunityData(DbContextContainer contextContainer)
        {
            this.Initialize(contextContainer);
        }

        /// <summary>
        /// The get all managed comunities.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public object GetAllManagedComunities(string userId = null)
        {
            var query = this.DbContext.Comunities
                .Include(c => c.President)
                .Include(c => c.DirectorBoardMembers);

            if (userId != null)
            {
                query = query.Where(c => c.DirectorBoardMembers.Any(db => db.ApplicationUser.Id == userId));
            }

            return query.ToList();
        }

        /// <summary>
        /// The get all managed comunities select.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public object GetAllManagedComunitiesSelect(string userId = null)
        {
            var query = this.DbContext.Comunities
                .Include(c => c.President)
                .Include(c => c.DirectorBoardMembers);

            if (userId != null)
            {
                query = query.Where(c => c.DirectorBoardMembers.Any(db => db.ApplicationUser.Id == userId));
            }

            var list = query.ToList();

            list.Insert(0, new Comunity() { Name = Strings.PleaseSelect });

            return new SelectList(list, "Id", "Name");
        }

        /// <summary>
        /// The save.
        /// </summary>
        /// <param name="comunity">
        /// The comunity.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool Save(Comunity comunity)
        {
            try
            {
                if (comunity != null)
                {
                    this.DbContext.Entry(comunity).State = EntityState.Modified;

                    this.DbContext.SaveChanges();
                }

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        /// <summary>
        /// The get comunity.
        /// </summary>
        /// <param name="comunityId">
        /// The comunity id.
        /// </param>
        /// <param name="insludePresident">
        /// The inslude president.
        /// </param>
        /// <param name="includeProperties">
        /// The include properties.
        /// </param>
        /// <returns>
        /// The <see cref="Comunity"/>.
        /// </returns>
        public Comunity GetComunity(Guid? comunityId, bool insludePresident = true, bool includeProperties = false)
        {
            if (comunityId != null && comunityId != Guid.Empty)
            {
                var query = this.DbContext.Comunities.Where(c => c.Id == comunityId);

                if (insludePresident)
                {
                    query = query.Include(c => c.President).Include(c => c.President.ApplicationUser)
                        .Include(c => c.President.ManagedComunities);
                }

                if (includeProperties)
                {
                    query = query.Include(c => c.Properties);
                }

                return query.Single();
            }

            return null;
        }

        /// <summary>
        /// The cahnge status.
        /// </summary>
        /// <param name="comunity">
        /// The comunity.
        /// </param>
        public void CahngeStatus(Comunity comunity)
        {
            comunity.Status = (comunity.Status == Status.Activate) ? Status.Deactivate : Status.Activate;
        }
    }
}