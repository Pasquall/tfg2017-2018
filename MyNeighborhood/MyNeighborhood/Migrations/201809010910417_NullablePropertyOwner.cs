// --------------------------------------------------------------------------------------------------------------------
// <copyright file="201809010910417_NullablePropertyOwner.cs" company="MyNeighborhood">
//   Pascual Maestre. MyNeihborhood.
// </copyright>
// <summary>
//   Defines the NullablePropertyOwner type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MyNeighborhood.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    /// <summary>
    /// The nullable property owner.
    /// </summary>
    public partial class NullablePropertyOwner : DbMigration
    {
        /// <summary>
        /// The up.
        /// </summary>
        public override void Up()
        {
            this.DropForeignKey("dbo.Properties", "OwnerId", "dbo.UserInfoes");
            this.DropIndex("dbo.Properties", new[] { "OwnerId" });
            this.AlterColumn("dbo.Properties", "OwnerId", c => c.Int());
            this.CreateIndex("dbo.Properties", "OwnerId");
            this.AddForeignKey("dbo.Properties", "OwnerId", "dbo.UserInfoes", "Id");
        }

        /// <summary>
        /// The down.
        /// </summary>
        public override void Down()
        {
            this.DropForeignKey("dbo.Properties", "OwnerId", "dbo.UserInfoes");
            this.DropIndex("dbo.Properties", new[] { "OwnerId" });
            this.AlterColumn("dbo.Properties", "OwnerId", c => c.Int(nullable: false));
            this.CreateIndex("dbo.Properties", "OwnerId");
            this.AddForeignKey("dbo.Properties", "OwnerId", "dbo.UserInfoes", "Id", cascadeDelete: true);
        }
    }
}
