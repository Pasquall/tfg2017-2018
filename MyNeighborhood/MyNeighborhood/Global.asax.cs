﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Global.asax.cs" company="MyNeighborhood">
//   Pascual Maestre. MyNeighborhood.
// </copyright>
// <summary>
//   The mvc application.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MyNeighborhood
{
    using System.Web.Mvc;
    using System.Web.Optimization;
    using System.Web.Routing;

    using MyNeighborhood.AppHelpers;
    using MyNeighborhood.Models;

    /// <summary>
    /// The mvc application.
    /// </summary>
    public class MvcApplication : System.Web.HttpApplication
    {
        /// <summary>
        /// The application_ start.
        /// </summary>
        protected void Application_Start()
        {
            ApplicationDbContext context = new ApplicationDbContext();

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            IdentityHelper.SeedIdentities(context);
        }
    }
}
