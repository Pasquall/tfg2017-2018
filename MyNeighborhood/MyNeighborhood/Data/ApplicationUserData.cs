﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ApplicationUserData.cs" company="MyNeighborhood">
//   Pascual Maestre. MyNeighborhood.
// </copyright>
// <summary>
//   The application user data.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MyNeighborhood.Data
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Web.Mvc;

    using MyNeighborhood.App_GlobalResources;
    using MyNeighborhood.Models;

    /// <summary>
    /// The application user data.
    /// </summary>
    public class ApplicationUserData : AbstractMyNeighborhoodData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationUserData"/> class.
        /// </summary>
        /// <param name="contextContainer">
        /// The context container.
        /// </param>
        public ApplicationUserData(DbContextContainer contextContainer)
        {
            this.Initialize(contextContainer);
        }

        #region Models

        /// <summary>
        /// The get all users.
        /// </summary>
        /// <returns>
        /// The <see cref="ICollection&lt;ApplicationUser&gt;"/>.
        /// </returns>
        public ICollection<ApplicationUser> GetAllUsers()
        {
            return this.DbContext.Users.Include(u => u.UserInfo).ToList();
        }

        /// <summary>
        /// The get all users by comunity.
        /// </summary>
        /// <param name="comunityId">
        /// The comunity id.
        /// </param>
        /// <returns>
        /// The <see cref="ICollection&lt;ApplicationUser&gt;"/>.
        /// </returns>
        public ICollection<ApplicationUser> GetAllUsersByComunity(Guid comunityId)
        {
            return this.DbContext.Users
                .Include(u => u.UserInfo)
                .Include(u => u.UserInfo.Properties)
                .Where(u => u.UserInfo.Properties.Any(p => p.ComunityId == comunityId))
                .ToList();
        }

        /// <summary>
        /// The get application user basic.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The <see cref="ApplicationUser"/>.
        /// </returns>
        public ApplicationUser GetApplicationUserBasic(string userId)
        {
            var applicationUserQuery = this.DbContext.Users
                .Where(u => u.Id == userId);

            return applicationUserQuery.Count() == 1 ? applicationUserQuery.First() : null;
        }

        /// <summary>
        /// The get application user basic.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The <see cref="ApplicationUser"/>.
        /// </returns>
        public ApplicationUser GetApplicationUserById(string userId)
        {
            var applicationUserQuery = this.DbContext.Users
                .Include(a => a.UserInfo)
                .Include(a => a.UserInfo.ManagedComunities)
                .Include(a => a.UserInfo.PresidedComunities)
                .Where(u => u.Id == userId);

            return applicationUserQuery.Count() == 1 ? applicationUserQuery.First() : null;
        }

        /// <summary>
        /// The save.
        /// </summary>
        /// <param name="appUser">
        /// The app user.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool Save(ApplicationUser appUser)
        {
            try
            {
                if (appUser != null)
                {
                    this.DbContext.Entry(appUser).State = EntityState.Modified;

                    this.DbContext.SaveChanges();
                }

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        #endregion

        /// <summary>
        /// The get presidentable users.
        /// </summary>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public object GetPresidentableUsers()
        {
            var adminRole = this.DbContext.Roles.SingleOrDefault(m => m.Name == RoleNames.RoleAdministrator);

            var query = this.DbContext.UserInfoes.Include(p => p.ApplicationUser)
                .Where(m => m.ApplicationUser.Roles.All(r => r.RoleId != adminRole.Id));

            var list = query.ToList();

            list.Insert(0, new UserInfo() { Name = Strings.PleaseSelect });

            return new SelectList(list, "Id", "Name");
        }

        /// <summary>
        /// The get presidentable users by counity.
        /// </summary>
        /// <param name="comunityId">
        /// The comunity id.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public object GetPresidentableUsersByCounity(Guid comunityId)
        {
            return new SelectList(this.DbContext.UserInfoes.Where(u => u.ManagedComunities.Any(c => c.Id == comunityId)), "Id", "Name");
        }

        /// <summary>
        /// The get all u sers select.
        /// </summary>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public object GetAllUsersSelect()
        {
            var query = this.DbContext.UserInfoes;

            var list = query.ToList();

            list.Insert(0, new UserInfo() { Name = Strings.PleaseSelect });

            return new SelectList(list, "Id", "Name");
        }

        /// <summary>
        /// The cahnge status.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        public void CahngeStatus(ApplicationUser user)
        {
            user.UserInfo.Status = (user.UserInfo.Status == Status.Activate) ? Status.Deactivate : Status.Activate;
        }
    }
}