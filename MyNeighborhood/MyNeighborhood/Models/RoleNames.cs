﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RoleNames.cs" company="MyNeighborhood">
//   Pascual Maestre. MyNeighborhood.
// </copyright>
// <summary>
//   Defines the RoleNames type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MyNeighborhood.Models
{
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    /// The role names.
    /// </summary>
    public class RoleNames
    {
        /// <summary>
        /// The role neighbor. (Vecino)
        /// </summary>
        public const string RoleNeighbor = "Neighbor";

        /// <summary>
        /// The role directors borard. (Juntadirectiva)
        /// </summary>
        public const string RoleDirectorsBorard = "DirectorBoards";

        /// <summary>
        /// The role administrator.
        /// </summary>
        public const string RoleAdministrator = "Administrator";

        /// <summary>
        /// The role maintenance.
        /// </summary>
        public const string RoleMaintenance = "Maintenance";

        /// <summary>
        /// The admin roles list.
        /// </summary>
        private readonly List<string> adminRolesList = new List<string> { RoleAdministrator, RoleDirectorsBorard, RoleMaintenance, RoleNeighbor };

        /// <summary>
        /// The directors board list.
        /// </summary>
        private readonly List<string> directorsBoardList = new List<string> { RoleDirectorsBorard, RoleMaintenance, RoleNeighbor };

        /// <summary>
        /// Gets or sets the admin roles list.
        /// </summary>
        /// <returns>
        /// The <see cref="List&lt;String&gt;"/>.
        /// </returns>
        public List<string> GetAdminRolesList()
        {
            return this.adminRolesList;
        }

        /// <summary>
        /// The get directors board list.
        /// </summary>
        /// <returns>
        /// The <see cref="List&lt;String&gt;"/>.
        /// </returns>
        public List<string> GetDirectorsBoardList()
        {
            return this.adminRolesList;
        }
    }
}