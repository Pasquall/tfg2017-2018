﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FilterConfig.cs" company="MyNeighborhood">
//   Pascual Maestre. MyNeigborhood
// </copyright>
// <summary>
//   The filter config.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MyNeighborhood
{
    using System.Diagnostics.CodeAnalysis;
    using System.Globalization;
    using System.Threading;
    using System.Web.Mvc;

    /// <summary>
    /// The filter config.
    /// </summary>
    public class FilterConfig
    {
        /// <summary>
        /// The register global filters.
        /// </summary>
        /// <param name="filters">
        /// The filters.
        /// </param>
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            
            // regist filter
            filters.Add(new LangFilterAttribute());
        }
    }

    /// <summary>
    /// The lang filter attribute.
    /// </summary>
    [SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1402:FileMayOnlyContainASingleClass", Justification = "Reviewed. Suppression is OK here.")]
    public class LangFilterAttribute : ActionFilterAttribute
    {
        /// <summary>
        /// The on action executing.
        /// </summary>
        /// <param name="filterContext">
        /// The filter context.
        /// </param>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext.HttpContext.Session["CurrentLang"] == null)
            {
                // default en
                filterContext.HttpContext.Session["CurrentLang"] = "en";
            }

            // change current cultureinfo every time
            Thread.CurrentThread.CurrentCulture =
                new CultureInfo(filterContext.HttpContext.Session["CurrentLang"].ToString());
            Thread.CurrentThread.CurrentUICulture =
                new CultureInfo(filterContext.HttpContext.Session["CurrentLang"].ToString());
            base.OnActionExecuting(filterContext);
        }
    }
}
