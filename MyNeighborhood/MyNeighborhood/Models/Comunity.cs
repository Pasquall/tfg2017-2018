﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Comunity.cs" company="MyNeigborhood">
//   Pascual Maestre. MyNeighborhood.
// </copyright>
// <summary>
//   Defines the Comunity type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MyNeighborhood.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// The comunity.
    /// </summary>
    public class Comunity
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the street adress.
        /// </summary>
        [Required]
        public string StreetAdress { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        [Required]
        public Status Status { get; set; }

        #region Relationship

        /// <summary>
        /// Gets or sets the properties of the user (houses, parking garages, ...)
        /// </summary>
        public ICollection<Property> Properties { get; set; }

        /// <summary>
        /// Gets or sets the director boards members.
        /// </summary>
        [InverseProperty("ManagedComunities")]
        public ICollection<UserInfo> DirectorBoardMembers { get; set; }

        /// <summary>
        /// Gets or sets the president id.
        /// </summary>
        [ForeignKey("President")]
        public int? PresidentId { get; set; }

        /// <summary>
        /// Gets or sets the president.
        /// </summary>
        public UserInfo President { get; set; }

        #endregion
    }
}