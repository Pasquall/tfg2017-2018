﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TypePropertyData.cs" company="MyNeighborhood">
//   Pascual Maestre. MyNeighborhood.
// </copyright>
// <summary>
//   The type property data.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MyNeighborhood.Data
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Web.Mvc;

    using MyNeighborhood.App_GlobalResources;
    using MyNeighborhood.Models;

    /// <summary>
    /// The type property data.
    /// </summary>
    public class TypePropertyData : AbstractMyNeighborhoodData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TypePropertyData"/> class.
        /// </summary>
        /// <param name="contextContainer">
        /// The context container.
        /// </param>
        public TypePropertyData(DbContextContainer contextContainer)
        {
            this.Initialize(contextContainer);
        }

        /// <summary>
        /// The get all type property.
        /// </summary>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public object GetAllTypePropertySelect()
        {
            var query = this.DbContext.TyeProperties;

            var list = query.ToList();

            list.Insert(0, new TypeProperty() { Name = Strings.PleaseSelect });

            return new SelectList(list, "Id", "Name");
        }

        /// <summary>
        /// The save.
        /// </summary>
        /// <param name="typeProperty">
        /// The type property.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool Save(TypeProperty typeProperty)
        {
            try
            {
                if (typeProperty != null)
                {
                    this.DbContext.Entry(typeProperty).State = EntityState.Modified;

                    this.DbContext.SaveChanges();
                }

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        /// <summary>
        /// The get property by id.
        /// </summary>
        /// <param name="id">
        /// The vm type property id.
        /// </param>
        /// <returns>
        /// The <see cref="TypeProperty"/>.
        /// </returns>
        public TypeProperty GetTypePropertyById(Guid id)
        {
            var query = this.DbContext.TyeProperties.Where(p => p.Id == id);

            if (query.Any())
            {
                return query.Single();
            }

            return null;
        }
    }
}