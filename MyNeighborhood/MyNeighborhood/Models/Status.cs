﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Status.cs" company="MyNeighborhood">
//   Pascual Maestre. My Neighborhood.
// </copyright>
// <summary>
//   The user status.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MyNeighborhood.Models
{
    /// <summary>
    /// The user status.
    /// </summary>
    public enum Status
    {
        /// <summary>
        /// The activate.
        /// </summary>
        Activate,

        /// <summary>
        /// The deactivate.
        /// </summary>
        Deactivate,
    }
}