﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IdentityHelper.cs" company="MyNeigborhood">
//   Pascual Maestre. MyNeighborhood.
// </copyright>
// <summary>
//   Defines the IdentityHelper type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MyNeighborhood.AppHelpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;

    using MyNeighborhood.Models;

    /// <summary>
    /// The identity helper.
    /// </summary>
    public class IdentityHelper
    {
        /// <summary>
        /// The seed identities.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        internal static void SeedIdentities(ApplicationDbContext context)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            #region roles

            if (!roleManager.RoleExists(RoleNames.RoleAdministrator))
            {
                var roleresult = roleManager.Create(new IdentityRole(RoleNames.RoleAdministrator));
            }

            if (!roleManager.RoleExists(RoleNames.RoleMaintenance))
            {
                var roleresult = roleManager.Create(new IdentityRole(RoleNames.RoleMaintenance));
            }

            if (!roleManager.RoleExists(RoleNames.RoleDirectorsBorard))
            {
                var roleresult = roleManager.Create(new IdentityRole(RoleNames.RoleDirectorsBorard));
            }

            if (!roleManager.RoleExists(RoleNames.RoleNeighbor))
            {
                var roleresult = roleManager.Create(new IdentityRole(RoleNames.RoleNeighbor));
            }

            #endregion

            #region DefaultComunities

            // Comunidad 1
            string comunityName = "MyNeighborhood";

            bool existComunity = context.Comunities.Any(i => i.Name == comunityName);
            if (!existComunity)
            {
                var comunity = new Comunity()
                {
                    Name = comunityName,
                    StreetAdress = "Calle falsa",
                    Status = Status.Activate,
                };
                context.Comunities.Add(comunity);
                context.SaveChanges();
            }

            // Comunidad 2
            string comunityName2 = "MyNeighborhood2";

            bool existComunity2 = context.Comunities.Any(i => i.Name == comunityName2);
            if (!existComunity2)
            {
                var comunity = new Comunity()
                {
                    Name = comunityName2,
                    StreetAdress = "Calle falsa",
                    Status = Status.Activate,
                };
                context.Comunities.Add(comunity);
                context.SaveChanges();
            }

            #endregion

            #region TypeProperties

            string typeName1 = "Piso";
            string typeName2 = "Garaje";

            bool existType1 = context.TyeProperties.Any(i => i.Name == typeName1);
            if (!existType1)
            {
                TypeProperty type1 = new TypeProperty()
                {
                    Name = typeName1,
                    Description = "Bloque de edificios",
                };
                context.TyeProperties.Add(type1);
                context.SaveChanges();
            }

            bool existType2 = context.TyeProperties.Any(i => i.Name == typeName2);
            if (!existType2)
            {
                TypeProperty type2 = new TypeProperty()
                {
                    Name = typeName2,
                    Description = "Plaza de garaje",
                };
                context.TyeProperties.Add(type2);
                context.SaveChanges();
            }

            #endregion

            #region Properties

            bool existProperties = context.Properties.Any();
            if (!existProperties)
            {
                Comunity comunity1 = context.Comunities.First(c => c.Name == comunityName);
                Comunity comunity2 = context.Comunities.First(c => c.Name == comunityName2);
                TypeProperty typeProperty1 = context.TyeProperties.First(t => t.Name == typeName1);
                TypeProperty typeProperty2 = context.TyeProperties.First(t => t.Name == typeName2);

                Property property1 = new Property()
                {
                    Door = "A",
                    Floor = "1",
                    Number = 22,
                    ComunityId = comunity1.Id,
                    TypePropertyId = typeProperty1.Id,
                };
                context.Properties.Add(property1);
                context.SaveChanges();

                Property property2 = new Property()
                {
                    Door = "B",
                    Floor = "1",
                    Number = 22,
                    ComunityId = comunity1.Id,
                    TypePropertyId = typeProperty1.Id,
                };
                context.Properties.Add(property2);
                context.SaveChanges();

                Property property3 = new Property()
                {
                    Door = "C",
                    Floor = "1",
                    Number = 22,
                    ComunityId = comunity1.Id,
                    TypePropertyId = typeProperty1.Id,
                };
                context.Properties.Add(property3);
                context.SaveChanges();

                Property property4 = new Property()
                {
                    Door = "3B",
                    Floor = "-1",
                    Number = 24,
                    ComunityId = comunity1.Id,
                    TypePropertyId = typeProperty2.Id,
                };
                context.Properties.Add(property4);
                context.SaveChanges();

                Property property5 = new Property()
                {
                    Door = "1C",
                    Floor = "-1",
                    Number = 24,
                    ComunityId = comunity1.Id,
                    TypePropertyId = typeProperty2.Id,
                };
                context.Properties.Add(property5);
                context.SaveChanges();

                Property property6 = new Property()
                {
                    Door = "H",
                    Floor = "1",
                    Number = 25,
                    ComunityId = comunity2.Id,
                    TypePropertyId = typeProperty1.Id,
                };
                context.Properties.Add(property6);
                context.SaveChanges();
            }

            #endregion

            #region DefaultUsers
            var password = "P@ssw0rd$tr0ng";

            // Administrador
            string adminName = "admin@neihborhood.com";

            ApplicationUser admin = userManager.FindByName(adminName);
            if (admin == null)
            {
                var userInfo = new UserInfo()
                {
                    Name = "admin",
                    Surname = "admin",
                    BirthDate = new DateTime(1992, 07, 28),
                    Status = Status.Activate,
                    RegisterDate = DateTime.Now,
                };
                context.UserInfoes.Add(userInfo);
                context.SaveChanges();

                var newNeighbor = new ApplicationUser()
                {
                    UserName = adminName,
                    Email = adminName,
                    UserInfo = userInfo,
                };

                var userResult = userManager.Create(newNeighbor, password);
                if (userResult.Succeeded)
                {
                    userManager.AddToRole(newNeighbor.Id, RoleNames.RoleAdministrator);
                }
            }

            // DirectorsBoard Comunity 1
            string directorsBoardName1 = "DirectorsBoard1@neihborhood.com";

            ApplicationUser directorsBoard1 = userManager.FindByName(directorsBoardName1);
            if (directorsBoard1 == null)
            {
                var comunity = context.Comunities.First(c => c.Name == comunityName);

                var userInfo = new UserInfo()
                {
                    Name = "DirectorsBoard1",
                    Surname = "DirectorsBoard",
                    BirthDate = new DateTime(1957, 08, 25),
                    Status = Status.Activate,
                    RegisterDate = DateTime.Now,
                    ManagedComunities = new List<Comunity> { comunity },
                };
                context.UserInfoes.Add(userInfo);
                context.SaveChanges();

                var newNeighbor = new ApplicationUser()
                {
                    UserName = directorsBoardName1,
                    Email = directorsBoardName1,
                    UserInfo = userInfo,
                };

                var userResult = userManager.Create(newNeighbor, password);
                if (userResult.Succeeded)
                {
                    userManager.AddToRole(newNeighbor.Id, RoleNames.RoleDirectorsBorard);
                }
            }

            // DirectorsBoard Comunity 2
            string directorsBoardName2 = "DirectorsBoard2@neihborhood.com";

            ApplicationUser directorsBoard2 = userManager.FindByName(directorsBoardName2);
            if (directorsBoard2 == null)
            {
                var comunity = context.Comunities.First(c => c.Name == comunityName2);

                var userInfo = new UserInfo()
                {
                    Name = "DirectorsBoard2",
                    Surname = "DirectorsBoard",
                    BirthDate = new DateTime(1987, 05, 25),
                    Status = Status.Activate,
                    RegisterDate = DateTime.Now,
                    ManagedComunities = new List<Comunity> { comunity },
                    PresidedComunities = new List<Comunity> { comunity },
                };
                context.UserInfoes.Add(userInfo);
                context.SaveChanges();

                var newNeighbor = new ApplicationUser()
                {
                    UserName = directorsBoardName2,
                    Email = directorsBoardName2,
                    UserInfo = userInfo,
                };

                var userResult = userManager.Create(newNeighbor, password);
                if (userResult.Succeeded)
                {
                    userManager.AddToRole(newNeighbor.Id, RoleNames.RoleDirectorsBorard);
                }
            }

            // Neighbor 1
            string neigborName1 = "neighbor1@neihborhood.com";

            ApplicationUser neighbor1 = userManager.FindByName(neigborName1);
            if (neighbor1 == null)
            {
                var userInfo = new UserInfo()
                {
                    Name = "Neighbor1",
                    Surname = "Neighbor",
                    BirthDate = new DateTime(1988, 10, 22),
                    Status = Status.Activate,
                    RegisterDate = DateTime.Now,
                };
                context.UserInfoes.Add(userInfo);
                context.SaveChanges();

                var newNeighbor = new ApplicationUser()
                {
                    UserName = neigborName1,
                    Email = neigborName1,
                    UserInfo = userInfo,
                };

                var userResult = userManager.Create(newNeighbor, password);
                if (userResult.Succeeded)
                {
                    userManager.AddToRole(newNeighbor.Id, RoleNames.RoleNeighbor);
                }

                var query = context.Properties.Where(p => !p.OwnerId.HasValue);
                if (query.Any())
                {
                    Property neighbor1Property = query.First();
                    neighbor1Property.OwnerId = newNeighbor.UserInfo.Id;
                    context.SaveChanges();
                }
            }

            // Neighbor 2
            string neigborName2 = "neighbor2@neihborhood.com";

            ApplicationUser neighbor2 = userManager.FindByName(neigborName2);
            if (neighbor1 == null)
            {
                var userInfo = new UserInfo()
                {
                    Name = "Neighbor2",
                    Surname = "Neighbor",
                    BirthDate = new DateTime(1952, 12, 12),
                    Status = Status.Activate,
                    RegisterDate = DateTime.Now,
                };
                context.UserInfoes.Add(userInfo);
                context.SaveChanges();

                var newNeighbor = new ApplicationUser()
                {
                    UserName = neigborName2,
                    Email = neigborName2,
                    UserInfo = userInfo,
                };

                var userResult = userManager.Create(newNeighbor, password);
                if (userResult.Succeeded)
                {
                    userManager.AddToRole(newNeighbor.Id, RoleNames.RoleNeighbor);
                }

                var query = context.Properties.Where(p => !p.OwnerId.HasValue);
                if (query.Any())
                {
                    Property neighbor2Property = query.First();
                    neighbor2Property.OwnerId = newNeighbor.UserInfo.Id;
                    context.SaveChanges();
                }
            }

            #endregion
        }
    }
}