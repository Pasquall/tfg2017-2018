﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Property.cs" company="MyNeighborhood">
//   Pascual Maestre. MyNeighborhood
// </copyright>
// <summary>
//   Defines the Property type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MyNeighborhood.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// The property.
    /// </summary>
    public class Property
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the number.
        /// </summary>
        [Required]
        public int Number { get; set; }

        /// <summary>
        /// Gets or sets the floor.
        /// </summary>
        public string Floor { get; set; }

        /// <summary>
        /// Gets or sets the door.
        /// </summary>
        public string Door { get; set; }

        #region RelationShips

        /// <summary>
        /// Gets or sets the owner id.
        /// </summary>
        [ForeignKey("UserInfo")]
        public int? OwnerId { get; set; }
        
        /// <summary>
        /// Gets or sets the user info.
        /// </summary>
        public UserInfo UserInfo { get; set; }

        /// <summary>
        /// Gets or sets the comunity id.
        /// </summary>
        [ForeignKey("Comunity")]
        [Required]
        public Guid ComunityId { get; set; }

        /// <summary>
        /// Gets or sets the house comunity.
        /// </summary>
        [Required]
        public Comunity Comunity { get; set; }

        /// <summary>
        /// Gets or sets the type property id.
        /// </summary>
        [ForeignKey("Type")]
        [Required]
        public Guid TypePropertyId { get; set; }
        
        /// <summary>
        /// Gets or sets the property type.
        /// </summary>
        [Required]
        public TypeProperty Type { get; set; }

        #endregion
    }
}