// --------------------------------------------------------------------------------------------------------------------
// <copyright file="201808281821147_AddUserInfoes_Property_And_Community.cs" company="MyNeighborhood">
//   Pascual Maestre. MyNeigborhood.
// </copyright>
// <summary>
//   The add user infoes_ property_ and_ community.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MyNeighborhood.Migrations
{
    using System.Data.Entity.Migrations;

    /// <summary>
    /// The add user infoes_ property_ and_ community.
    /// </summary>
    public partial class AddUserInfoes_Property_And_Community : DbMigration
    {
        /// <summary>
        /// The up.
        /// </summary>
        public override void Up()
        {
            this.CreateTable(
                "dbo.Comunities",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        StreetAdress = c.String(nullable: false),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);

            this.CreateTable(
                "dbo.Properties",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Number = c.Int(nullable: false),
                        Floor = c.String(),
                        Door = c.String(),
                        OwnerId = c.Int(nullable: false),
                        ComunityId = c.Guid(nullable: false),
                        TypePropertyId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Comunities", t => t.ComunityId, cascadeDelete: true)
                .ForeignKey("dbo.TypeProperties", t => t.TypePropertyId, cascadeDelete: true)
                .ForeignKey("dbo.UserInfoes", t => t.OwnerId, cascadeDelete: true)
                .Index(t => t.OwnerId)
                .Index(t => t.ComunityId)
                .Index(t => t.TypePropertyId);

            this.CreateTable(
                "dbo.TypeProperties",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);

            this.CreateTable(
                "dbo.UserInfoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Surname = c.String(nullable: false),
                        Status = c.Int(nullable: false),
                        BirthDate = c.DateTime(),
                        RegisterDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);

            this.AddColumn("dbo.AspNetUsers", "UserInfo_Id", c => c.Int());
            this.CreateIndex("dbo.AspNetUsers", "UserInfo_Id");
            this.AddForeignKey("dbo.AspNetUsers", "UserInfo_Id", "dbo.UserInfoes", "Id");
        }

        /// <summary>
        /// The down.
        /// </summary>
        public override void Down()
        {
            this.DropForeignKey("dbo.Properties", "OwnerId", "dbo.UserInfoes");
            this.DropForeignKey("dbo.AspNetUsers", "UserInfo_Id", "dbo.UserInfoes");
            this.DropForeignKey("dbo.Properties", "TypePropertyId", "dbo.TypeProperties");
            this.DropForeignKey("dbo.Properties", "ComunityId", "dbo.Comunities");
            this.DropIndex("dbo.AspNetUsers", new[] { "UserInfo_Id" });
            this.DropIndex("dbo.Properties", new[] { "TypePropertyId" });
            this.DropIndex("dbo.Properties", new[] { "ComunityId" });
            this.DropIndex("dbo.Properties", new[] { "OwnerId" });
            this.DropColumn("dbo.AspNetUsers", "UserInfo_Id");
            this.DropTable("dbo.UserInfoes");
            this.DropTable("dbo.TypeProperties");
            this.DropTable("dbo.Properties");
            this.DropTable("dbo.Comunities");
        }
    }
}
