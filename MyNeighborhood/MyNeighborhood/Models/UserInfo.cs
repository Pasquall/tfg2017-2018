﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserInfo.cs" company="MyNeighborhood">
//   Pascual Maestre MyNeighborhood.
// </copyright>
// <summary>
//   Defines the UserInfo type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MyNeighborhood.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Information about user not refrent to registration.
    /// </summary>
    public class UserInfo
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the surname.
        /// </summary>
        [Required]
        public string Surname { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        [Required]
        public Status Status { get; set; }

        /// <summary>
        /// Gets or sets the birth date.
        /// </summary>
        public DateTime? BirthDate { get; set; }

        /// <summary>
        /// Gets or sets the register date.
        /// </summary>
        public DateTime RegisterDate { get; set; }

        #region Relationships
        
        /// <summary>
        /// Gets or sets the properties of the user (houses, parking garages, ...)
        /// </summary>
        public ICollection<Property> Properties { get; set; }

        /// <summary>
        /// Gets or sets the user info.
        /// </summary>
        public ApplicationUser ApplicationUser { get; set; }

        /// <summary>
        /// Gets or sets the management comunity of this user.
        /// </summary>
        [InverseProperty("DirectorBoardMembers")]
        public ICollection<Comunity> ManagedComunities { get; set; } // TODO buscar directiva para poner muchos a muchos o cambiar esta parte por "Comunity ManagedComunity"

        /// <summary>
        /// Gets or sets the presided comunities.
        /// </summary>
        public ICollection<Comunity> PresidedComunities { get; set; }

        #endregion
    }
}