﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DbContextContainer.cs" company="MyNeihborhood">
//   Pascual Maestre. MyNeighborhood.
// </copyright>
// <summary>
//   The db context container.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MyNeighborhood.Data
{
    using MyNeighborhood.Models;

    /// <summary>
    /// The db context container.
    /// </summary>
    public class DbContextContainer
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DbContextContainer"/> class.
        /// </summary>
        public DbContextContainer()
        {
            this.DbContext = new ApplicationDbContext();

            this.InitializeDatas();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DbContextContainer"/> class.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        public DbContextContainer(ApplicationDbContext context)
        {
            this.DbContext = context;

            this.InitializeDatas();
        }

        /// <summary>
        /// Gets or sets the application user data.
        /// </summary>
        public ApplicationUserData ApplicationUserData { get; set; }

        /// <summary>
        /// Gets or sets the user data.
        /// </summary>
        public UserData UserData { get; set; }

        /// <summary>
        /// Gets or sets the comunity data.
        /// </summary>
        public ComunityData ComunityData { get; set; }

        /// <summary>
        /// Gets or sets the property data.
        /// </summary>
        public PropertyData PropertyData { get; set; }

        /// <summary>
        /// Gets or sets the type property data.
        /// </summary>
        public TypePropertyData TypePropertyData { get; set; }

        /// <summary>
        /// Gets or sets the db context.
        /// </summary>
        public ApplicationDbContext DbContext { get; set; }

        /// <summary>
        /// The new context.
        /// </summary>
        /// <returns>
        /// The <see cref="DbContextContainer"/>.
        /// </returns>
        public static DbContextContainer NewContext() => new DbContextContainer();

        /// <summary>
        /// The dispose.
        /// </summary>
        public void Dispose()
        {
            this.DbContext.Dispose();
        }

        /// <summary>
        /// The initialize datas.
        /// </summary>
        private void InitializeDatas()
        {
            this.ApplicationUserData = new ApplicationUserData(this);
            this.UserData = new UserData(this);
            this.ComunityData = new ComunityData(this);
            this.PropertyData = new PropertyData(this);
            this.TypePropertyData = new TypePropertyData(this);
        }
    }
}