﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AbstractMyNeighborhoodData.cs" company="MyNeyhborhood">
//   Pascual Maestre. MyNeihborhood.
// </copyright>
// <summary>
//   Defines the AbstractMyNeighborhoodData type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MyNeighborhood.Data
{
    using MyNeighborhood.Models;

    /// <summary>
    /// The abstract my neighborhood data.
    /// </summary>
    public class AbstractMyNeighborhoodData
    {
        /// <summary>
        /// Gets or sets the db context.
        /// </summary>
        protected ApplicationDbContext DbContext { get; set; }

        /// <summary>
        /// Gets or sets the db context container.
        /// </summary>
        protected DbContextContainer DbContextContainer { get; set; }

        /// <summary>
        /// The initialize.
        /// </summary>
        /// <param name="contextContainer">
        /// The context container.
        /// </param>
        protected void Initialize(DbContextContainer contextContainer)
        {
            this.DbContextContainer = contextContainer;
            this.DbContext = contextContainer.DbContext;
        }
    }
}