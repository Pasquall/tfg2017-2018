﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MyNeighborhood.cs" company="MyNeighborhood">
//   Pascual Maestre. MyNeighborhood.
// </copyright>
// <summary>
//   The my neighborhood.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MyNeighborhood.Controllers
{
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Web;
    using System.Web.Mvc;

    using Microsoft.AspNet.Identity.Owin;

    using global::MyNeighborhood.Data;
    using global::MyNeighborhood.Models;

    using Microsoft.AspNet.Identity;

    /// <summary>
    /// The my neighborhood.
    /// </summary>
    [SuppressMessage("StyleCop.CSharp.OrderingRules", "SA1202:ElementsMustBeOrderedByAccess", Justification = "Reviewed. Suppression is OK here.")]
    [SuppressMessage("StyleCop.CSharp.OrderingRules", "SA1201:ElementsMustAppearInTheCorrectOrder", Justification = "Reviewed. Suppression is OK here.")]
    [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Reviewed. Suppression is OK here.")]
    [SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1401:FieldsMustBePrivate", Justification = "Reviewed. Suppression is OK here.")]
    [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1309:FieldNamesMustNotBeginWithUnderscore", Justification = "Reviewed. Suppression is OK here.")]
    public class MyNeighborhood : Controller
    {
        /// <summary>
        /// The db.
        /// </summary>
        protected ApplicationDbContext Db => this.DbContextContainer.DbContext;

        /// <summary>
        /// The db context container.
        /// </summary>
        protected readonly DbContextContainer DbContextContainer = new DbContextContainer();

        /// <summary>
        /// The _user manager.
        /// </summary>
        protected ApplicationUserManager _userManager;


        /// <summary>
        /// The _sign in manager.
        /// </summary>
        protected ApplicationSignInManager _signInManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="MyNeighborhood"/> class. 
        /// </summary>
        /// <param name="userManager">
        /// The user manager.
        /// </param>
        /// <param name="signInManager">
        /// The sign in manager.
        /// </param>
        public MyNeighborhood(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            this.UserManager = userManager;
            this.SignInManager = signInManager;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MyNeighborhood"/> class. 
        /// </summary>
        protected MyNeighborhood()
        {
        }

        /// <summary>
        /// Gets the sign in manager.
        /// </summary>
        public ApplicationSignInManager SignInManager
        {
            get => this._signInManager ?? this.HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            private set => this._signInManager = value;
        }

        /// <summary>
        /// Gets the user manager.
        /// </summary>
        public ApplicationUserManager UserManager
        {
            get => this._userManager ?? this.HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            private set => this._userManager = value;
        }


        /// <summary>
        /// The add errors.
        /// </summary>
        /// <param name="result">
        /// The result.
        /// </param>
        protected void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                this.ModelState.AddModelError(string.Empty, error);
            }
        }

        /// <summary>
        /// The add errors.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="result">
        /// The result.
        /// </param>
        protected void AddErrors(string key, IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                this.ModelState.AddModelError(key, error);
            }
        }

        /// <summary>
        /// The add errors.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="message">
        /// The message.
        /// </param>
        protected void AddErrors(string key, string message)
        {
            this.ModelState.AddModelError(key, message);
        }

        /// <summary>
        /// The redirect to local.
        /// </summary>
        /// <param name="returnUrl">
        /// The return url.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        protected ActionResult RedirectToLocal(string returnUrl)
        {
            if (this.Url.IsLocalUrl(returnUrl))
            {
                return this.Redirect(returnUrl);
            }

            return this.RedirectToAction("Index", "Home");
        }


        /// <summary>
        /// The get user role.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        protected string GetUserRole(string userId)
        {
            var result = string.Empty;

            if (this.UserManager.IsInRole(userId, RoleNames.RoleAdministrator))
            {
                result = RoleNames.RoleAdministrator;
            }
            if (this.UserManager.IsInRole(userId, RoleNames.RoleDirectorsBorard))
            {
                result = RoleNames.RoleDirectorsBorard;
            }
            if (this.UserManager.IsInRole(userId, RoleNames.RoleMaintenance))
            {
                result = RoleNames.RoleMaintenance;
            }
            if (this.UserManager.IsInRole(userId, RoleNames.RoleNeighbor))
            {
                result = RoleNames.RoleNeighbor;
            }

            return result;
        }

        /// <summary>
        /// The change user role.
        /// </summary>
        /// <param name="applicationUser">
        /// The application user.
        /// </param>
        /// <param name="role">
        /// The role.
        /// </param>
        protected void ChangeUserRole(ApplicationUser applicationUser, string role)
        {
            foreach (var itemRole in applicationUser.Roles)
            {
                this.UserManager.RemoveFromRole(applicationUser.Id, this.GetUserRole(applicationUser.Id));
            }

            this.UserManager.AddToRole(applicationUser.Id, role);

            this.DbContextContainer.ApplicationUserData.Save(applicationUser);
        }
    }
}