﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EditPropertyViewModel.cs" company="MyNeighborhood">
//   Pascual Maestre. MyNeighborhood
// </copyright>
// <summary>
//   The edit property view model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MyNeighborhood.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// The edit property view model.
    /// </summary>
    public class EditPropertyViewModel
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the number.
        /// </summary>
        [Required]
        public int Num { get; set; }

        /// <summary>
        /// Gets or sets the floor.
        /// </summary>
        public string Floor { get; set; }

        /// <summary>
        /// Gets or sets the door.
        /// </summary>
        public string Door { get; set; }

        /// <summary>
        /// Gets or sets the type property id.
        /// </summary>
        [Required]
        public Guid TypePropertyId { get; set; }

        /// <summary>
        /// Gets or sets the comunity id.
        /// </summary>
        [Required]
        public Guid ComunityId { get; set; }

        /// <summary>
        /// Gets or sets the owner id.
        /// </summary>
        public int? OwnerId { get; set; }
    }
}