﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PropertyController.cs" company="MyNeighborhood">
//   Pascual MAestre. MyNeighborhood.
// </copyright>
// <summary>
//   Defines the PropertiesController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MyNeighborhood.Controllers
{
    using System;
    using System.Data.Entity;
    using System.Net;
    using System.Web.Mvc;

    using global::MyNeighborhood.App_GlobalResources;
    using global::MyNeighborhood.Models;

    using Microsoft.AspNet.Identity;

    /// <summary>
    /// The properties controller.
    /// </summary>
    [Authorize]
    public class PropertyController : MyNeighborhood
    {
        /// GET: Properties
        /// <summary>
        /// The index.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [Authorize(Roles = RoleNames.RoleAdministrator)]
        public ActionResult Index()
        {
            var isAdmin = this.User.IsInRole(RoleNames.RoleAdministrator);

            if (!isAdmin)
            {
                return this.HttpNotFound();
            }

            var properties = this.DbContextContainer.PropertyData.GetAllProperties();
            return this.View(properties);
        }

        /// <summary>
        /// The index comunity.
        /// </summary>
        /// <param name="comunityId">
        /// The comuniti id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [Authorize(Roles = RoleNames.RoleAdministrator + ", " + RoleNames.RoleDirectorsBorard)]
        public ActionResult IndexComunity(Guid? comunityId)
        {
            if (comunityId != null && comunityId != Guid.Empty)
            {
                var comunity = this.DbContextContainer.ComunityData.GetComunity(comunityId);
                var isAdmin = this.User.IsInRole(RoleNames.RoleAdministrator);
                var isDirectorsBoardOfComunit = this.User.IsInRole(RoleNames.RoleDirectorsBorard)
                                                 && this.DbContextContainer.ApplicationUserData.GetApplicationUserById(this.User.Identity.GetUserId()).UserInfo.ManagedComunities.Contains(comunity);

                if (!isAdmin && !isDirectorsBoardOfComunit)
                {
                    return this.HttpNotFound();
                }

                this.ViewBag.ReturnUrl = this.Url.Action("Edit", "Comunities", new { id = comunityId });
                
                var properties = this.DbContextContainer.PropertyData.GetAllPropertiesByComunity(comunityId);
                return this.PartialView("Index", properties);
            }

            return this.PartialView("Index");
        }

        /// <summary>
        /// The index neihbor.
        /// </summary>
        /// <param name="neighborId">
        /// The neighbor id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [Authorize(Roles = RoleNames.RoleNeighbor)]
        public ActionResult IndexNeihbor(string neighborId)
        {
            var isNeighbor = this.UserManager.IsInRole(neighborId, RoleNames.RoleNeighbor);

            if (isNeighbor)
            {
                var properties = this.DbContextContainer.PropertyData.GetAllPropertiesByNeighbor(neighborId);

                return this.View("Index", properties);
            }

            return this.View("Index");
        }

        /// GET: Properties/Create
        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="returnUrl">
        /// The return Url.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [Authorize(Roles = RoleNames.RoleAdministrator + ", " + RoleNames.RoleDirectorsBorard)]
        public ActionResult Create(string returnUrl)
        {
            this.ViewBag.ReturnUrl = returnUrl;
            
            var isAdmin = this.User.IsInRole(RoleNames.RoleAdministrator);

            this.ViewBag.ComunityId = this.DbContextContainer.ComunityData.GetAllManagedComunitiesSelect(!isAdmin ? this.User.Identity.GetUserId() : null);
            this.ViewBag.TypePropertyId = this.DbContextContainer.TypePropertyData.GetAllTypePropertySelect();
            this.ViewBag.OwnerId = this.DbContextContainer.ApplicationUserData.GetAllUsersSelect();

            return this.View();
        }

        /// POST: Properties/Create
        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="vm">
        /// The vm.
        /// </param>
        /// <param name="returnUrl">
        /// The return Url.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = RoleNames.RoleAdministrator + ", " + RoleNames.RoleDirectorsBorard)]
        public ActionResult Create(EditPropertyViewModel vm, string returnUrl)
        {
            this.ViewBag.ReturnUrl = returnUrl;
            
            Comunity propertyComunity = this.DbContextContainer.ComunityData.GetComunity(vm.ComunityId);
            var isAdmin = this.User.IsInRole(RoleNames.RoleAdministrator);

            if (propertyComunity != null)
            {
                var isDirectorsBoardComunity = this.User.IsInRole(RoleNames.RoleDirectorsBorard)
                                               && (this.DbContextContainer.ApplicationUserData
                                                       .GetApplicationUserById(this.User.Identity.GetUserId()).UserInfo
                                                       .ManagedComunities.Contains(propertyComunity) || this
                                                       .DbContextContainer.ApplicationUserData
                                                       .GetApplicationUserById(this.User.Identity.GetUserId()).UserInfo
                                                       .PresidedComunities.Contains(propertyComunity));

                if (isAdmin || isDirectorsBoardComunity)
                {
                    if (this.ModelState.IsValid)
                    {
                        try
                        {
                            Property comunity = this.PropertyVmToNewProperty(vm);
                            this.DbContextContainer.DbContext.Properties.Add(comunity);
                            this.DbContextContainer.DbContext.SaveChanges();

                            return this.RedirectToIndex(returnUrl);
                        }
                        catch (Exception e)
                        {
                            this.ViewBag.ComunityId = this.DbContextContainer.ComunityData.GetAllManagedComunitiesSelect(this.User.Identity.GetUserId());
                            this.ViewBag.TypePropertyId = this.DbContextContainer.TypePropertyData.GetAllTypePropertySelect();
                            this.ViewBag.OwnerId = this.DbContextContainer.ApplicationUserData.GetAllUsersSelect();
                            return this.View(vm);
                        }
                    }
                }
            }
            else
            {
                this.ModelState.AddModelError("TypePropertyId", Strings.RequiredField);
                this.ModelState.AddModelError("ComunityId", Strings.RequiredField);
            }

            this.ViewBag.ComunityId = this.DbContextContainer.ComunityData.GetAllManagedComunitiesSelect(!isAdmin ? this.User.Identity.GetUserId() : null);
            this.ViewBag.TypePropertyId = this.DbContextContainer.TypePropertyData.GetAllTypePropertySelect();
            this.ViewBag.OwnerId = this.DbContextContainer.ApplicationUserData.GetAllUsersSelect();
            return this.View(vm);
        }

        /// GET: Properties/Edit/5
        /// <summary>
        /// The edit.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="returnUrl">
        /// The return Url.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [Authorize(Roles = RoleNames.RoleAdministrator + ", " + RoleNames.RoleDirectorsBorard)]
        public ActionResult Edit(Guid? id, string returnUrl)
        {
            this.ViewBag.ReturnUrl = returnUrl;

            var isAdmin = this.User.IsInRole(RoleNames.RoleAdministrator);

            if (id == null || id == Guid.Empty)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Property property = this.DbContextContainer.PropertyData.GetPropertyById(id);

            if (property == null)
            {
                return this.HttpNotFound();
            }

            var isDirectorsBoardComunity = this.User.IsInRole(RoleNames.RoleDirectorsBorard)
                                           && (this.DbContextContainer.ApplicationUserData.GetApplicationUserById(this.User.Identity.GetUserId()).UserInfo.ManagedComunities.Contains(property.Comunity)
                                               || this.DbContextContainer.ApplicationUserData.GetApplicationUserById(this.User.Identity.GetUserId()).UserInfo.PresidedComunities.Contains(property.Comunity));

            if (!isAdmin && !isDirectorsBoardComunity)
            {
                return this.HttpNotFound();
            }

            EditPropertyViewModel vm = this.PropertyModelToVm(property);

            this.ViewBag.ComunityId = this.DbContextContainer.ComunityData.GetAllManagedComunitiesSelect(!isAdmin ? this.User.Identity.GetUserId() : null);
            this.ViewBag.TypePropertyId = this.DbContextContainer.TypePropertyData.GetAllTypePropertySelect();
            this.ViewBag.OwnerId = this.DbContextContainer.ApplicationUserData.GetAllUsersSelect();
            return this.View(vm);
        }

        /// POST: Properties/Edit/5
        /// <summary>
        /// The edit.
        /// </summary>
        /// <param name="vm">
        /// The property.
        /// </param>
        /// <param name="returnUrl">
        /// The return Url.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = RoleNames.RoleAdministrator + ", " + RoleNames.RoleDirectorsBorard)]
        public ActionResult Edit(EditPropertyViewModel vm, string returnUrl)
        {
            this.ViewBag.ReturnUrl = returnUrl;
            
            var isAdmin = this.User.IsInRole(RoleNames.RoleAdministrator);

            if (this.ModelState.IsValid)
            {
                if (vm == null)
                {
                    return this.HttpNotFound();
                }

                Comunity comunity = this.DbContextContainer.ComunityData.GetComunity(vm.ComunityId);

                var isDirectorsBoardComunity = this.User.IsInRole(RoleNames.RoleDirectorsBorard)
                                               && (this.DbContextContainer.ApplicationUserData.GetApplicationUserById(this.User.Identity.GetUserId()).UserInfo.ManagedComunities.Contains(comunity)
                                                   || this.DbContextContainer.ApplicationUserData.GetApplicationUserById(this.User.Identity.GetUserId()).UserInfo.PresidedComunities.Contains(comunity));

                if (!isAdmin && !isDirectorsBoardComunity)
                {
                    return this.HttpNotFound();
                }

                Property property = this.PropertyVmToModel(vm);

                this.DbContextContainer.PropertyData.Save(property);
                return this.RedirectToIndex(returnUrl);
            }

            this.ViewBag.ComunityId = this.DbContextContainer.ComunityData.GetAllManagedComunitiesSelect(!isAdmin ? this.User.Identity.GetUserId() : null);
            this.ViewBag.TypePropertyId = this.DbContextContainer.TypePropertyData.GetAllTypePropertySelect();
            this.ViewBag.OwnerId = this.DbContextContainer.ApplicationUserData.GetAllUsersSelect();
            return this.View(vm);
        }

        /// GET: Properties/Delete/5
        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="returnUrl">
        /// The return Url.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [Authorize(Roles = RoleNames.RoleAdministrator + ", " + RoleNames.RoleDirectorsBorard)]
        public ActionResult Delete(Guid? id, string returnUrl)
        {
            if (id == null || id == Guid.Empty)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var property = this.DbContextContainer.PropertyData.GetPropertyById(id);
            this.DbContextContainer.DbContext.Properties.Remove(property);
            this.DbContextContainer.DbContext.SaveChanges();

            return this.RedirectToIndex(returnUrl);
        }

        /// <summary>
        /// The property vm to new property.
        /// </summary>
        /// <param name="vm">
        /// The vm.
        /// </param>
        /// <returns>
        /// The <see cref="Property"/>.
        /// </returns>
        private Property PropertyVmToNewProperty(EditPropertyViewModel vm)
        {
            return new Property()
            {
                Id = new Guid(),
                Number = vm.Num,
                Floor = vm.Floor,
                Door = vm.Door,
                Type = this.DbContextContainer.TypePropertyData.GetTypePropertyById(vm.TypePropertyId),
                Comunity = this.DbContextContainer.ComunityData.GetComunity(vm.ComunityId),
                UserInfo = this.DbContextContainer.UserData.GetUserInfoById(vm.OwnerId),
            };
        }

        /// <summary>
        /// The property model to vm.
        /// </summary>
        /// <param name="property">
        /// The property.
        /// </param>
        /// <returns>
        /// The <see cref="EditPropertyViewModel"/>.
        /// </returns>
        private EditPropertyViewModel PropertyModelToVm(Property property)
        {
            return new EditPropertyViewModel()
            {
                Id = property.Id,
                Num = property.Number,
                Floor = property.Floor,
                Door = property.Door,
                TypePropertyId = property.Type.Id,
                ComunityId = property.Comunity.Id,
                OwnerId = property.UserInfo?.Id,
            };
        }

        /// <summary>
        /// The property vm to model.
        /// </summary>
        /// <param name="vm">
        /// The vm.
        /// </param>
        /// <returns>
        /// The <see cref="Property"/>.
        /// </returns>
        private Property PropertyVmToModel(EditPropertyViewModel vm)
        {
            var property = this.DbContextContainer.PropertyData.GetPropertyById(vm.Id);

            if (property != null)
            {
                property.Number = vm.Num;
                property.Door = vm.Door;
                property.Floor = vm.Floor;
                property.Comunity = this.DbContextContainer.ComunityData.GetComunity(vm.ComunityId);
                property.UserInfo = this.DbContextContainer.UserData.GetUserInfoById(vm.OwnerId);
                property.Type = this.DbContextContainer.TypePropertyData.GetTypePropertyById(vm.TypePropertyId);

                return property;
            }

            return null;
        }

        /// <summary>
        /// The redirect to index.
        /// </summary>
        /// <param name="returnUrl">
        /// The return url.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        private ActionResult RedirectToIndex(string returnUrl)
        {
            if (this.Url.IsLocalUrl(returnUrl))
            {
                return this.Redirect(returnUrl);
            }

            return this.RedirectToAction("Index");
        }
    }
}
