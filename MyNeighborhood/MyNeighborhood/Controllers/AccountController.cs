﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AccountController.cs" company="MyNeighborhood">
// Pascual Maestre. MyNeighborhood.
// </copyright>
// <summary>
//   Defines the AccountController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MyNeighborhood.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Security;

    using global::MyNeighborhood.App_GlobalResources;

    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.Owin;
    using Microsoft.Owin.Security;

    using global::MyNeighborhood.Models;

    using WebGrease.Css.Extensions;

    /// <summary>
    /// The account controller.
    /// </summary>
    [Authorize]
    public class AccountController : MyNeighborhood
    {
        /// GET: /Account/Login
        /// <summary>
        /// The login.
        /// </summary>
        /// <param name="returnUrl">
        /// The return url.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            this.ViewBag.ReturnUrl = returnUrl;
            return this.View();
        }

        /// POST: /Account/Login
        /// <summary>
        /// The login.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <param name="returnUrl">
        /// The return url.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(model);
            }

            var user = this.UserManager.FindByEmail(model.Email);
            if (user != null)
            {
                user = this.DbContextContainer.ApplicationUserData.GetApplicationUserById(user.Id);

                if (user.UserInfo.Status == Status.Activate)
                {
                    var result = await this.SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);
                    switch (result)
                    {
                        case SignInStatus.Success:
                            return this.RedirectToLocal(returnUrl);
                        case SignInStatus.LockedOut:
                            return this.View("Lockout");
                        case SignInStatus.RequiresVerification:
                        case SignInStatus.Failure:
                            this.ModelState.AddModelError("Email", Strings.IncorrectUserOrPasword);
                            return this.View(model);
                        default:
                            this.ModelState.AddModelError(string.Empty, Strings.LoginAccesNotValid);
                            return this.View(model);
                    }
                }
                else
                {
                    this.ModelState.AddModelError("Email", Strings.UserDisabled);
                }
            }
            else
            {
                this.ModelState.AddModelError("Email", Strings.IncorrectUserOrPasword);
            }

            return this.View(model);
        }

        /// GET: /Account/RegisterUser
        /// <summary>
        /// The register.
        /// </summary>
        /// <param name="returnUrl">
        /// The return Url.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [AllowAnonymous]
        public ActionResult RegisterUser(string returnUrl)
        {
            this.ViewBag.isRegisterView = true;
            this.ViewBag.ReturnUrl = returnUrl;
            
            var isAdmin = this.User.IsInRole(RoleNames.RoleAdministrator);
            var isDirectorsBoard = this.User.IsInRole(RoleNames.RoleDirectorsBorard);

            RoleNames roleNames = new RoleNames();
            var listRolesUser = isAdmin ? roleNames.GetAdminRolesList() : isDirectorsBoard ? roleNames.GetDirectorsBoardList() : new List<string>();

            EditViewModel model = new EditViewModel()
            {
                ListRoleNames = listRolesUser,
            };

            return this.View(model);
        }

        /// POST: /Account/RegisterUser
        /// <summary>
        /// The register.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <param name="returnUrl">
        /// The return Url.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RegisterUser(EditViewModel model, string returnUrl)
        {
            this.ViewBag.isRegisterView = true;
            this.ViewBag.ReturnUrl = returnUrl;

            var isPermissionUser = this.GetIsPermissionUser(model.Role);

            if (this.ModelState.IsValid && (isPermissionUser || !this.User.Identity.IsAuthenticated))
            {
                var appUser = new ApplicationUser { UserName = model.Email, Email = model.Email, };
                var userInfo = new UserInfo
                {
                    Name = model.Name,
                    Surname = model.Surname,
                    BirthDate = model.BirthDate,
                    Status = Status.Activate,
                    RegisterDate = DateTime.Now,
                };
                var result = await this.UserManager.CreateAsync(appUser, model.Password);
                if (result.Succeeded)
                {
                    // Add PersonInfo
                    appUser = this.DbContextContainer.ApplicationUserData.GetApplicationUserBasic(appUser.Id);
                    userInfo = this.DbContextContainer.UserData.CreateInfoes(userInfo);

                    appUser.UserInfo = userInfo;
                    this.DbContextContainer.UserData.Save(appUser);

                    var newRole = (string.IsNullOrEmpty(model.Role) || !this.User.Identity.IsAuthenticated) ? RoleNames.RoleNeighbor : model.Role;
                    this.UserManager.AddToRole(appUser.Id, newRole);

                    if (!this.Request.IsAuthenticated)
                    {
                        await this.SignInManager.SignInAsync(appUser, isPersistent: false, rememberBrowser: false);

                        return this.RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return this.RedirectToIndex(returnUrl);
                    }

                    // Para obtener más información sobre cómo habilitar la confirmación de cuentas y el restablecimiento de contraseña, visite https://go.microsoft.com/fwlink/?LinkID=320771
                    // Enviar correo electrónico con este vínculo
                    // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                    // await UserManager.SendEmailAsync(user.Id, "Confirmar cuenta", "Para confirmar la cuenta, haga clic <a href=\"" + callbackUrl + "\">aquí</a>");
                }

                this.AddErrors("Name", result);
            }

            if (model.ListRoleNames == null || !model.ListRoleNames.Any())
            {
                var isAdmin = this.User.IsInRole(RoleNames.RoleAdministrator);
                var isDirectorsBoard = this.User.IsInRole(RoleNames.RoleDirectorsBorard);

                RoleNames roleNames = new RoleNames();
                var listRolesUser = isAdmin ? roleNames.GetAdminRolesList() : isDirectorsBoard ? roleNames.GetDirectorsBoardList() : new List<string>();

                model.ListRoleNames = listRolesUser;
            }

            // Si llegamos a este punto, es que se ha producido un error y volvemos a mostrar el formulario
            return this.View(model);
        }

        /// <summary>
        /// The edit user.
        /// </summary>
        /// <param name="userId">
        /// The user Id.
        /// </param>
        /// <param name="returnUrl">
        /// The return Url.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [Authorize(Roles = RoleNames.RoleAdministrator + ", " + RoleNames.RoleNeighbor + ", " + RoleNames.RoleDirectorsBorard)]
        public ActionResult EditUser(string userId, string returnUrl)
        {
            this.ViewBag.isRegisterView = false;
            this.ViewBag.ReturnUrl = returnUrl;

            var isAdmin = this.User.IsInRole(RoleNames.RoleAdministrator);
            var isDirectorsBoard = this.User.IsInRole(RoleNames.RoleDirectorsBorard);

            RoleNames roleNames = new RoleNames();
            var listRolesUser = isAdmin ? roleNames.GetAdminRolesList() : isDirectorsBoard ? roleNames.GetDirectorsBoardList() : new List<string>();

            ApplicationUser user = this.DbContextContainer.ApplicationUserData.GetApplicationUserById(userId);

            EditViewModel model = null;
            if (user != null && user.UserInfo.Status == Status.Activate)
            {
                var accessUserId = this.User.Identity.GetUserId();
                var isDirectornBoard = this.User.IsInRole(RoleNames.RoleDirectorsBorard);
                var isOwnerProfile = userId == accessUserId;

                if (this.ModelState.IsValid && (isOwnerProfile || isAdmin || isDirectornBoard))
                {
                    model = new EditViewModel()
                    {
                        Id = user.Id,
                        Name = user.UserInfo.Name,
                        Surname = user.UserInfo.Surname,
                        Email = user.Email,
                        BirthDate = user.UserInfo.BirthDate,
                        Role = this.GetUserRole(user.Id),
                        ListRoleNames = listRolesUser,
                    };
                }

                return this.View(model);
            }

            return this.HttpNotFound();
        }

        /// <summary>
        /// The edit user.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <param name="returnUrl">
        /// The return Url.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = RoleNames.RoleAdministrator + ", " + RoleNames.RoleNeighbor + ", " + RoleNames.RoleDirectorsBorard)]
        public ActionResult EditUser(EditPersonalInfoViewModel model, string returnUrl)
        {
            this.ViewBag.isRegisterView = false;
            this.ViewBag.ReturnUrl = returnUrl;
            
            var isAdmin = this.User.IsInRole(RoleNames.RoleAdministrator);
            var isDirectorsBoard = this.User.IsInRole(RoleNames.RoleDirectorsBorard);

            RoleNames roleNames = new RoleNames();
            var listRolesUser = isAdmin ? roleNames.GetAdminRolesList() : isDirectorsBoard ? roleNames.GetDirectorsBoardList() : new List<string>();

            var returnModel = new EditViewModel()
            {
                Id = model.Id,
                Name = model.Name,
                Surname = model.Surname,
                Email = model.Email,
                BirthDate = model.BirthDate,
                Role = model.Role,
                ListRoleNames = listRolesUser,
            };

            var accessUserId = this.User.Identity.GetUserId();
            var isOwnerProfile = model.Id == accessUserId;

            if (this.ModelState.IsValid && (isOwnerProfile || isAdmin || isDirectorsBoard))
            {
                var applicationUser = this.EditModelToAppUser(model);

                if (applicationUser != null)
                {
                    this.ChangeUserRole(applicationUser, model.Role);
                }
            }

            return this.View(returnModel);
        }

        /// <summary>
        /// The list of comunity users (users withs properties in the comunity).
        /// </summary>
        /// <param name="comunityId">
        /// The comunity Id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [Authorize(Roles = RoleNames.RoleAdministrator + ", " + RoleNames.RoleDirectorsBorard)]
        public ActionResult IndexComunity(Guid comunityId)
        {
            var user = this.DbContextContainer.ApplicationUserData.GetApplicationUserById(this.User.Identity.GetUserId());

            var managedComunities = user.UserInfo.ManagedComunities;
            var presidedComunities = user.UserInfo.PresidedComunities;

            if (managedComunities.Any(m => m.Id == comunityId) || presidedComunities.Any(p => p.Id == comunityId))
            {
                var users = this.DbContextContainer.ApplicationUserData.GetAllUsersByComunity(comunityId);

                this.ViewBag.ReturnUrl = this.Url.Action("Edit", "Comunities", new { id = comunityId });

                return this.PartialView("Index", users);
            }

            return this.HttpNotFound();
        }

        /// <summary>
        /// The list of all users in the app.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [Authorize(Roles = RoleNames.RoleAdministrator)]
        public ActionResult Index()
        {
            var users = this.DbContextContainer.ApplicationUserData.GetAllUsers();

            return this.View(users);
        }

        /// <summary>
        /// The change status.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <param name="returnUrl">
        /// The return Url.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [Authorize(Roles = RoleNames.RoleAdministrator + ", " + RoleNames.RoleDirectorsBorard)]
        public ActionResult ChangeStatus(string userId, string returnUrl)
        {
            var user = this.DbContextContainer.ApplicationUserData.GetApplicationUserById(userId);

            if (user != null && userId != this.User.Identity.GetUserId())
            {
                this.DbContextContainer.ApplicationUserData.CahngeStatus(user);

                this.DbContextContainer.ApplicationUserData.Save(user);
            }

            return this.RedirectToIndex(returnUrl);
        }

        /// GET: /Account/ConfirmEmail
        /// <summary>
        /// The confirm email.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <param name="code">
        /// The code.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return this.View("Error");
            }

            var result = await this.UserManager.ConfirmEmailAsync(userId, code);
            return this.View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        /// GET: /Account/ForgotPassword
        /// <summary>
        /// The forgot password.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return this.View();
        }

        /// POST: /Account/ForgotPassword
        /// <summary>
        /// The forgot password.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [SuppressMessage("StyleCop.CSharp.SpacingRules", "SA1027:TabsMustNotBeUsed", Justification = "Reviewed. Suppression is OK here.")]
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (this.ModelState.IsValid)
            {
                var user = await this.UserManager.FindByNameAsync(model.Email);
                if (user == null || !(await this.UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    // No revelar que el usuario no existe o que no está confirmado
                    return this.View("ForgotPasswordConfirmation");
                }

                // Para obtener más información sobre cómo habilitar la confirmación de cuentas y el restablecimiento de contraseña, visite https://go.microsoft.com/fwlink/?LinkID=320771
                // Enviar correo electrónico con este vínculo
                // string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                // var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);	
                // await UserManager.SendEmailAsync(user.Id, "Restablecer contraseña", "Para restablecer la contraseña, haga clic <a href=\"" + callbackUrl + "\">aquí</a>");
                // return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }

            // Si llegamos a este punto, es que se ha producido un error y volvemos a mostrar el formulario
            return this.View(model);
        }

        /// GET: /Account/ForgotPasswordConfirmation
        /// <summary>
        /// The forgot password confirmation.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return this.View();
        }

        /// POST: /Account/LogOff
        /// <summary>
        /// The log off.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            this.AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return this.RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// The dispose.
        /// </summary>
        /// <param name="disposing">
        /// The disposing.
        /// </param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this._userManager != null)
                {
                    this._userManager.Dispose();
                    this._userManager = null;
                }

                if (this._signInManager != null)
                {
                    this._signInManager.Dispose();
                    this._signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #region viewModelToDataModel

        /// <summary>
        /// The edit model to app user.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="ApplicationUser"/>.
        /// </returns>
        private ApplicationUser EditModelToAppUser(EditPersonalInfoViewModel model)
        {
            var appUser = this.DbContextContainer.ApplicationUserData.GetApplicationUserById(model.Id);

            if (appUser == null || appUser.UserInfo.Status != Status.Activate)
            {
                return null;
            }

            appUser.UserInfo.Name = model.Name;
            appUser.UserInfo.Surname = model.Surname;
            appUser.UserInfo.BirthDate = model.BirthDate;
            appUser.Email = model.Email;

            return appUser;
        }

        #endregion

        /// <summary>
        /// The get is permission user.
        /// </summary>
        /// <param name="newRole">
        /// The new role.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool GetIsPermissionUser(string newRole)
        {
            var isAdmin = this.User.IsInRole(RoleNames.RoleAdministrator);
            var isDirectorsBoard = this.User.IsInRole(RoleNames.RoleDirectorsBorard);

            bool result = newRole != null || isAdmin || (isDirectorsBoard && newRole != RoleNames.RoleAdministrator);

            return result;
        }

        /// <summary>
        /// The redirect to index.
        /// </summary>
        /// <param name="returnUrl">
        /// The return url.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        private ActionResult RedirectToIndex(string returnUrl)
        {
            if (this.Url.IsLocalUrl(returnUrl))
            {
                return this.Redirect(returnUrl);
            }

            return this.RedirectToAction("Index");
        }

        #region Aplicaciones auxiliares
        /// <summary>
        /// Se usa para la protección XSRF al agregar inicios de sesión externos
        /// </summary>
        private const string XsrfKey = "XsrfId";

        /// <summary>
        /// Gets the authentication manager.
        /// </summary>
        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return this.HttpContext.GetOwinContext().Authentication;
            }
        }

        /// <summary>
        /// The challenge result.
        /// </summary>
        internal class ChallengeResult : HttpUnauthorizedResult
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="ChallengeResult"/> class.
            /// </summary>
            /// <param name="provider">
            /// The provider.
            /// </param>
            /// <param name="redirectUri">
            /// The redirect uri.
            /// </param>
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="ChallengeResult"/> class.
            /// </summary>
            /// <param name="provider">
            /// The provider.
            /// </param>
            /// <param name="redirectUri">
            /// The redirect uri.
            /// </param>
            /// <param name="userId">
            /// The user id.
            /// </param>
            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                this.LoginProvider = provider;
                this.RedirectUri = redirectUri;
                this.UserId = userId;
            }

            /// <summary>
            /// Gets or sets the login provider.
            /// </summary>
            public string LoginProvider { get; set; }

            /// <summary>
            /// Gets or sets the redirect uri.
            /// </summary>
            public string RedirectUri { get; set; }

            /// <summary>
            /// Gets or sets the user id.
            /// </summary>
            public string UserId { get; set; }

            /// <summary>
            /// The execute result.
            /// </summary>
            /// <param name="context">
            /// The context.
            /// </param>
            [SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1126:PrefixCallsCorrectly", Justification = "Reviewed. Suppression is OK here.")]
            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = this.RedirectUri };
                if (this.UserId != null)
                {
                    properties.Dictionary[XsrfKey] = this.UserId;
                }

                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, this.LoginProvider);
            }
        }
        #endregion
    }
}