﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ComunitiesController.cs" company="MyNeighborhood">
//   Pascual Maestre. MyNeighborhood.
// </copyright>
// <summary>
//   Defines the ComunitiesController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MyNeighborhood.Controllers
{
    using System;
    using System.Data.Entity;
    using System.Net;
    using System.Web.Mvc;

    using global::MyNeighborhood.Models;

    using Microsoft.AspNet.Identity;

    /// <summary>
    /// The comunities controller.
    /// </summary>
    [Authorize]
    public class ComunitiesController : MyNeighborhood
    {
        /// GET: Comunities
        /// <summary>
        /// The index.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [Authorize(Roles = RoleNames.RoleAdministrator + ", " + RoleNames.RoleDirectorsBorard)]
        public ActionResult Index()
        {
            var isAdmin = this.User.IsInRole(RoleNames.RoleAdministrator);

            var comunities = this.DbContextContainer.ComunityData.GetAllManagedComunities(isAdmin ? null : this.User.Identity.GetUserId());

            return this.View(comunities);
        }

        /// GET: Comunities/Create
        /// <summary>
        /// The create.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [Authorize(Roles = RoleNames.RoleAdministrator)]
        public ActionResult Create()
        {
            var isAdmin = this.User.IsInRole(RoleNames.RoleAdministrator);

            if (!isAdmin)
            {
                return this.HttpNotFound();
            }

            this.ViewBag.PresidentId = this.DbContextContainer.ApplicationUserData.GetPresidentableUsers();

            return this.View();
        }

        /// POST: Comunities/Create
        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="vm">
        /// The vm.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = RoleNames.RoleAdministrator)]
        public ActionResult Create(EditComunityViewModel vm)
        {
            var isAdmin = this.User.IsInRole(RoleNames.RoleAdministrator);

            if (this.ModelState.IsValid && isAdmin)
            {
                Comunity comunity = this.ComunityVmToNewComunity(vm);
                this.DbContextContainer.DbContext.Comunities.Add(comunity);
                this.DbContextContainer.DbContext.SaveChanges();

                comunity = this.DbContextContainer.ComunityData.GetComunity(comunity.Id);

                if (comunity.President != null)
                {
                    this.ChangeUserRole(comunity.President.ApplicationUser, RoleNames.RoleDirectorsBorard);
                    comunity.President.ManagedComunities.Add(comunity);
                    comunity.President.PresidedComunities.Add(comunity);
                }

                this.DbContextContainer.DbContext.SaveChanges();

                return this.RedirectToAction("Index");
            }

            this.ViewBag.PresidentId = this.ViewBag.PresidentId = this.DbContextContainer.ApplicationUserData.GetPresidentableUsers();
            return this.View(vm);
        }

        /// GET: Comunities/Edit/5
        /// <summary>
        /// The edit.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [Authorize(Roles = RoleNames.RoleAdministrator + ", " + RoleNames.RoleDirectorsBorard)]
        public ActionResult Edit(Guid? id)
        {
            if (id == null || id == Guid.Empty)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Comunity comunity = this.DbContextContainer.ComunityData.GetComunity(id);

            if (comunity == null || comunity.Status == Status.Deactivate)
            {
                return this.HttpNotFound();
            }

            var isAdmin = this.User.IsInRole(RoleNames.RoleAdministrator);
            var isDirectorsBoardComunity = this.User.IsInRole(RoleNames.RoleDirectorsBorard)
                                   && (this.DbContextContainer.ApplicationUserData.GetApplicationUserById(this.User.Identity.GetUserId()).UserInfo.ManagedComunities.Contains(comunity)
                                   || this.DbContextContainer.ApplicationUserData.GetApplicationUserById(this.User.Identity.GetUserId()).UserInfo.PresidedComunities.Contains(comunity));

            if (!isAdmin && !isDirectorsBoardComunity)
            {
                return this.HttpNotFound();
            }

            EditComunityViewModel vm = this.ComunityModelToVm(comunity);

            this.ViewBag.PresidentId = this.DbContextContainer.ApplicationUserData.GetPresidentableUsersByCounity(comunity.Id);
            return this.View(vm);

        }

        /// POST: Comunities/Edit/5
        /// <summary>
        /// The edit.
        /// </summary>
        /// <param name="vm">
        /// The vm.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = RoleNames.RoleAdministrator + ", " + RoleNames.RoleDirectorsBorard)]
        public ActionResult Edit(EditComunityViewModel vm)
        {
            if (this.ModelState.IsValid)
            {
                Comunity comunity = this.ComunityVmToModel(vm);

                var isAdmin = this.User.IsInRole(RoleNames.RoleAdministrator);
                var isDirectorsBoardComunity = this.User.IsInRole(RoleNames.RoleDirectorsBorard)
                                               && (this.DbContextContainer.ApplicationUserData.GetApplicationUserById(this.User.Identity.GetUserId()).UserInfo.ManagedComunities.Contains(comunity)
                                                   || this.DbContextContainer.ApplicationUserData.GetApplicationUserById(this.User.Identity.GetUserId()).UserInfo.PresidedComunities.Contains(comunity));

                if ((!isAdmin && !isDirectorsBoardComunity) || comunity.Status == Status.Deactivate)
                {
                    return this.HttpNotFound();
                }

                this.DbContextContainer.ComunityData.Save(comunity);
                return this.RedirectToAction("Index");
            }
            this.ViewBag.PresidentId = this.DbContextContainer.ApplicationUserData.GetPresidentableUsersByCounity(vm.Id);
            return this.View(vm);
        }

        /// <summary>
        /// The change status.
        /// </summary>
        /// <param name="id">
        /// The user id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [Authorize(Roles = RoleNames.RoleAdministrator)]
        public ActionResult ChangeStatus(Guid? id)
        {
            if (id != null && id != Guid.Empty)
            {
                var isAdmin = this.User.IsInRole(RoleNames.RoleAdministrator);

                var comunity = this.DbContextContainer.ComunityData.GetComunity(id);

                if (comunity != null && isAdmin)
                {
                    this.DbContextContainer.ComunityData.CahngeStatus(comunity);

                    this.DbContextContainer.ComunityData.Save(comunity);
                }
            }

            return this.RedirectToAction("Index");
        }

        /// <summary>
        /// The dispose.
        /// </summary>
        /// <param name="disposing">
        /// The disposing.
        /// </param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.DbContextContainer.Dispose();
            }
            base.Dispose(disposing);
        }

        /// <summary>
        /// The comunity vm to model.
        /// </summary>
        /// <param name="vm">
        /// The vm.
        /// </param>
        /// <returns>
        /// The <see cref="Comunity"/>.
        /// </returns>
        private Comunity ComunityVmToModel(EditComunityViewModel vm)
        {
            var comunity = this.DbContextContainer.ComunityData.GetComunity(vm.Id);

            if (comunity != null && comunity.Status == Status.Activate)
            {
                comunity.Name = vm.Name;
                comunity.StreetAdress = vm.StreetAdress;

                if (comunity.President != null)
                {
                    comunity.President.ManagedComunities.Remove(comunity);
                    comunity.President.PresidedComunities.Remove(comunity);
                }

                comunity.PresidentId = vm.PresidentId;
                comunity.President?.ManagedComunities.Add(comunity);
                comunity.President?.PresidedComunities.Add(comunity);

                return comunity;
            }

            return null;
        }

        /// <summary>
        /// The comunity vm to new cmunity.
        /// </summary>
        /// <param name="vm">
        /// The vm.
        /// </param>
        /// <returns>
        /// The <see cref="Comunity"/>.
        /// </returns>
        private Comunity ComunityVmToNewComunity(EditComunityViewModel vm)
        {
            return new Comunity()
            {
                Id = new Guid(),
                Name = vm.Name,
                StreetAdress = vm.StreetAdress,
                President = this.DbContextContainer.UserData.GetUserInfoById(vm.PresidentId),
                Status = Status.Activate,
            };
        }

        /// <summary>
        /// The comunity model to vm.
        /// </summary>
        /// <param name="comunity">
        /// The comunity.
        /// </param>
        /// <returns>
        /// The <see cref="EditComunityViewModel"/>.
        /// </returns>
        private EditComunityViewModel ComunityModelToVm(Comunity comunity)
        {
            return new EditComunityViewModel()
            {
                Id = comunity.Id,
                Name = comunity.Name,
                StreetAdress = comunity.StreetAdress,
                PresidentId = comunity.PresidentId,
                Status = Status.Activate,
            };
        }

    }
}
