﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IdentityModels.cs" company="MyNeighborhood">
//   Pascual Maestre. MyNeighbrhood.
// </copyright>
// <summary>
//   The application user.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MyNeighborhood.Models
{
    using System.Data.Entity;
    using System.Diagnostics.CodeAnalysis;
    using System.Security.Claims;
    using System.Threading.Tasks;

    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;

    /// Para agregar datos de perfil del usuario, agregue más propiedades a su clase ApplicationUser. Visite https://go.microsoft.com/fwlink/?LinkID=317594 para obtener más información.
    /// <summary>
    /// The application user.
    /// </summary>
    public class ApplicationUser : IdentityUser
    {
        /// <summary>
        /// Gets or sets the user info.
        /// </summary>
        public UserInfo UserInfo { get; set; }

        /// <summary>
        /// The generate user identity async.
        /// </summary>
        /// <param name="manager">
        /// The manager.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Tenga en cuenta que el valor de authenticationType debe coincidir con el definido en CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            
            // Agregar aquí notificaciones personalizadas de usuario
            return userIdentity;
        }
    }

    /// <summary>
    /// The application db context.
    /// </summary>
    [SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1402:FileMayOnlyContainASingleClass", Justification = "Reviewed. Suppression is OK here.")]
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationDbContext"/> class.
        /// </summary>
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }
        
        /// <summary>
        /// Gets or sets the user info.
        /// </summary>
        public DbSet<UserInfo> UserInfoes { get; set; }

        /// <summary>
        /// Gets or sets the property.
        /// </summary>
        public DbSet<Property> Properties { get; set; }

        /// <summary>
        /// Gets or sets the tye property.
        /// </summary>
        public DbSet<TypeProperty> TyeProperties { get; set; }

        /// <summary>
        /// Gets or sets the user info.
        /// </summary>
        public DbSet<Comunity> Comunities { get; set; }

        /// <summary>
        /// The create.
        /// </summary>
        /// <returns>
        /// The <see cref="ApplicationDbContext"/>.
        /// </returns>
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        /// <summary>
        /// The on model creating.
        /// </summary>
        /// <param name="modelBuilder">
        /// The model builder.
        /// </param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<ApplicationUser>().HasOptional(au => au.UserInfo).WithOptionalDependent(aui => aui.ApplicationUser).WillCascadeOnDelete(false);
        }
    }
}