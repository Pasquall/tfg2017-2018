// --------------------------------------------------------------------------------------------------------------------
// <copyright file="201809010907216_NullablePresident.cs" company="MyNeighborhood">
//   Pascual Maestre. MyNeihborhood.
// </copyright>
// <summary>
//   Defines the NullablePresident type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MyNeighborhood.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    /// <summary>
    /// The nullable president.
    /// </summary>
    public partial class NullablePresident : DbMigration
    {
        /// <summary>
        /// The up.
        /// </summary>
        public override void Up()
        {
            this.DropForeignKey("dbo.Comunities", "PresidentId", "dbo.UserInfoes");
            this.DropIndex("dbo.Comunities", new[] { "PresidentId" });
            this.AlterColumn("dbo.Comunities", "PresidentId", c => c.Int());
            this.CreateIndex("dbo.Comunities", "PresidentId");
            this.AddForeignKey("dbo.Comunities", "PresidentId", "dbo.UserInfoes", "Id");
        }

        /// <summary>
        /// The down.
        /// </summary>
        public override void Down()
        {
            this.DropForeignKey("dbo.Comunities", "PresidentId", "dbo.UserInfoes");
            this.DropIndex("dbo.Comunities", new[] { "PresidentId" });
            this.AlterColumn("dbo.Comunities", "PresidentId", c => c.Int(nullable: false));
            this.CreateIndex("dbo.Comunities", "PresidentId");
            this.AddForeignKey("dbo.Comunities", "PresidentId", "dbo.UserInfoes", "Id", cascadeDelete: true);
        }
    }
}
