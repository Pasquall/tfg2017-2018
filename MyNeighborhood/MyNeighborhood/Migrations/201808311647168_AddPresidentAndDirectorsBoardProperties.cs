// --------------------------------------------------------------------------------------------------------------------
// <copyright file="201808311647168_AddPresidentAndDirectorsBoardProperties.cs" company="MyNeighborhood">
//   Pascual Maestre. MyNeighborhood
// </copyright>
// <summary>
//   The add president and directors board properties.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MyNeighborhood.Migrations
{
    using System.Data.Entity.Migrations;

    /// <summary>
    /// The add president and directors board properties.
    /// </summary>
    public partial class AddPresidentAndDirectorsBoardProperties : DbMigration
    {
        /// <summary>
        /// The up.
        /// </summary>
        public override void Up()
        {
            this.CreateTable(
                "dbo.ComunityUserInfoes",
                c => new
                    {
                        Comunity_Id = c.Guid(nullable: false),
                        UserInfo_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Comunity_Id, t.UserInfo_Id })
                .ForeignKey("dbo.Comunities", t => t.Comunity_Id, cascadeDelete: true)
                .ForeignKey("dbo.UserInfoes", t => t.UserInfo_Id, cascadeDelete: true)
                .Index(t => t.Comunity_Id)
                .Index(t => t.UserInfo_Id);

            this.AddColumn("dbo.Comunities", "PresidentId", c => c.Int(nullable: false));
            this.CreateIndex("dbo.Comunities", "PresidentId");
            this.AddForeignKey("dbo.Comunities", "PresidentId", "dbo.UserInfoes", "Id", cascadeDelete: false);
        }

        /// <summary>
        /// The down.
        /// </summary>
        public override void Down()
        {
            this.DropForeignKey("dbo.Comunities", "PresidentId", "dbo.UserInfoes");
            this.DropForeignKey("dbo.ComunityUserInfoes", "UserInfo_Id", "dbo.UserInfoes");
            this.DropForeignKey("dbo.ComunityUserInfoes", "Comunity_Id", "dbo.Comunities");
            this.DropIndex("dbo.ComunityUserInfoes", new[] { "UserInfo_Id" });
            this.DropIndex("dbo.ComunityUserInfoes", new[] { "Comunity_Id" });
            this.DropIndex("dbo.Comunities", new[] { "PresidentId" });
            this.DropColumn("dbo.Comunities", "PresidentId");
            this.DropTable("dbo.ComunityUserInfoes");
        }
    }
}
