﻿window.JsApp = (new function(window, document) {
    this.self = this;

    this.changeLanguaje = (li, languaje) => {
        var $this = $(li);
        var idLang = $($this).data("global");
        $.ajax({
            url: languaje,
            type: "post",
            data: { lang: idLang },
            success: result => {
                if (result === "0") {
                    alert("change language failed");
                } else {
                    window.location.reload(idLang);
                }
            }
        });
    };
}(window, document));

$.validator.methods.date = function (value, element) {
    return this.optional(element) || !isNaN(Date.parse(value.replace(/([0-9]+)\/([0-9]+)/, '$2/$1')));
};