﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MyNeighborhood.Startup))]
namespace MyNeighborhood
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
