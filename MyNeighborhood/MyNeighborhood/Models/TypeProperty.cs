﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TypeProperty.cs" company="MyNeighborhood">
//   Pascual Maestre. MyNeighorhood.
// </copyright>
// <summary>
//   Defines the TypeProperty type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MyNeighborhood.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// The type property.
    /// </summary>
    public class TypeProperty
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public string Description { get; set; }

        #region RelationShip

        /// <summary>
        /// Gets or sets the properties of the user (houses, parking garages, ...)
        /// </summary>
        public ICollection<Property> Properties { get; set; }

        #endregion
    }
}